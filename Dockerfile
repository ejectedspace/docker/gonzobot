FROM python:3.5.6-stretch

RUN apt-get update
RUN apt-get install -y git libenchant-dev libxml2-dev libxslt-dev zlib1g-dev
RUN git clone https://github.com/snoonetIRC/CloudBot.git --branch gonzobot --single-branch /gonzobot

WORKDIR /gonzobot

RUN pip install -r requirements.txt
COPY ./config.json .
CMD ["python", "-m", "cloudbot"]
